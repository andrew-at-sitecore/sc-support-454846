﻿using Sitecore.Publishing.Pipelines.PublishItem;
using System.Diagnostics;

namespace Sitecore.Support.Diagnostics.Publishing.Pipelines.PublishItem
{
    public class BeginMeasurement : PublishItemProcessor
    {
        public override void Process(PublishItemContext context)
        {
            Stopwatch sw = new Stopwatch();
            context.CustomData.Add("Measurement_Stopwatch", sw);
            sw.Start();
        }
    }
}
