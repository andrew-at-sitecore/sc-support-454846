﻿using Sitecore.Data.Items;
using Sitecore.Publishing.Diagnostics;
using Sitecore.Publishing.Pipelines.PublishItem;
using System.Diagnostics;

namespace Sitecore.Support.Diagnostics.Publishing.Pipelines.PublishItem
{
    class StopMeasurement : PublishItemProcessor
    {
        public override void Process(PublishItemContext context)
        {
            Stopwatch sw = context.CustomData["Measurement_Stopwatch"] as Stopwatch;           
            if (sw != null)
            {
                sw.Stop();
                Item itemToPublish = context.PublishHelper.GetItemToPublish(context.ItemId);
                string name = (itemToPublish != null) ? itemToPublish.Name : "(null)";
                string fullPath = (itemToPublish != null) ? itemToPublish.Paths.FullPath : "(null)";
                string uri = (itemToPublish != null) ? itemToPublish.Uri.ToString() : "(null)";
                PublishingLog.Info(string.Format("##Sitecore Support: Publish Item Measurement: FullPath='{0}', Uri='{1}', Time Spent(ms)={2}", new object[] { fullPath, uri, sw.ElapsedMilliseconds }), null);
            }
        }
    }
}
